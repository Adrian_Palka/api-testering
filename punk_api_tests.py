import requests


def test_default_beers():
    response = requests.get("https://api.punkapi.com/v2/beers")
    body = response.json()
    assert body[0]["id"] == 1
    assert body[-1]["id"] == 25
    assert len(body) == 25


def test_ids_from_11_to_20():
    params = {
        "ids": "11|12|13|14|15|16|17|18|19|20"
    }
    response = requests.get("https://api.punkapi.com/v2/beers?", params=params)
    body = response.json()
    assert len(body) == 10

    beer_id = 11
    for beer in body:
        assert beer["id"] == beer_id
        beer_id += 1


def test_id_123():
    response = requests.get("https://api.punkapi.com/v2/beers/123")
    body = response.json()
    assert body[0]["id"] == 123


def test_20_results_from_5th_page():
    response = requests.get("https://api.punkapi.com/v2/beers?page=5&per_page=20")
    body = response.json()
    assert body[0]["id"] == 81
    assert body[-1]["id"] == 100
    assert len(body) == 20


def test_abv_results_from_5_to_7():
    response = requests.get("https://api.punkapi.com/v2/beers/?abv_gt=4.9&abv_lt=7.1")
    body = response.json()
    for beer in body:
        assert 5.0 > beer["abv"] <= 7.0


def test_beers_produced_in_2010():
    response = requests.get("https://api.punkapi.com/v2/beers?brewed_after=12-2009&brewed_before=01-2011")
    body = response.json()
    assert body["first_brewed"]
