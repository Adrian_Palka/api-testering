from faker import Faker
from utils.gorest_handler_CRUD import GoRESTHandler

gorest_handler = GoRESTHandler()


def test_create_user():
    # Using Faker to generate user name & email.
    generated_user_by_faker = Faker().name_male()
    random_integer_mail = Faker().random_int(min=0, max=150000)

    user_data = {
        "name": generated_user_by_faker,
        "gender": "male",
        "email": f"{generated_user_by_faker}@example".replace(" ", "").lower(),
        "status": "active"
    }

    new_user_data = {
        "name": "Johny Bravo",
        "gender": "male",
        "email": f"johny_bravo{random_integer_mail}@example".replace(" ", ""),
        "status": "active"
    }
    # 1. Creating a new user.
    body = gorest_handler.create_user(user_data).json()
    assert "id" in body
    user_id = body["id"]

    # 2. User data verification.
    body = gorest_handler.get_user(user_id).json()
    assert body["email"] == user_data["email"]
    assert body["name"] == user_data["name"]
    # assert user_id == user_data['id']

    # 3. User data update.
    body = gorest_handler.patch_user(user_id, new_user_data).json()
    assert body["name"] == new_user_data["name"]
    assert body["email"] == new_user_data["email"]

    # 4. Deleting a user.
    body = gorest_handler.delete_user(user_id)
    assert not "id" in body
    assert not "name" in body
    assert not "email" in body
