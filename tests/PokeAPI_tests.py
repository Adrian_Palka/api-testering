import requests
from utils.pokeapi_handler import PokeAPIHandler

pokeapi_handler = PokeAPIHandler()


def tests_of_the_default_list_of_pokemons():
    response = pokeapi_handler.get_list_of_pokemons()
    #    response = requests.get("https://pokeapi.co/api/v2/pokemon")
    body = response.json()

    assert len(body["results"]) > 0
    assert body["results"]

    #   assert "results" in body -> check only if list exist
    assert response.status_code == 200

    assert body["count"] == 1281

    response_time_ms = response.elapsed.microseconds // 1000
    assert response_time_ms < 1000

    response_size_bytes = len(response.content)
    assert response_size_bytes < 100000


def test_pagination():
    params = {
        "limit": 11,
        "offset": 21
    }
    response = pokeapi_handler.get_list_of_pokemons(params)
    #    response = requests.get("https://pokeapi.co/api/v2/pokemon", params=params)
    body = response.json()

    assert len(body["results"]) == params["limit"]

    assert body["results"][0]["url"] == f"https://pokeapi.co/api/v2/pokemon/{params['offset'] + 1}/"
    assert body["results"][-1]["url"] == f"https://pokeapi.co/api/v2/pokemon/{params['offset'] + params['limit']}/"


def test_shapes():
    body = pokeapi_handler.get_shapes_of_pokemons().json()

    assert body["count"] == len(body["results"])
    pokemon_shape = body["results"][2]["name"]
    body = pokeapi_handler.get_shapes_of_pokemons(pokemon_shape).json()
    assert body["id"] == 3
